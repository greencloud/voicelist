#!/bin/bash
# 输出网址
echo "http://127.0.0.1:8000"
# 杀死已存在的MMVCServerSIO.py进程
ps -ef | grep 'MMVCServerSIO.py' | grep -v grep | awk '{print $2}' | xargs -r kill -9

# 切换到服务器的工作目录
cd /mnt/workspace/voice-changer/server

# 使用nohup在后台启动服务器，并忽略所有输出
nohup python MMVCServerSIO.py -p 8000