#!/bin/bash

cd /kaggle/working/
python3 -m pip install colorama --quiet
mkdir Hmod
cd Hmod
git clone https://github.com/w-okada/voice-changer.git --depth=1 --quiet .
echo -e "\033[32m> Successfully cloned the repository!\033[0m"
cd server
sed -i "s/-.-.-.-/Kaggle.Mod/" '../client/demo/dist/assets/gui_settings/version.txt'
mv MMVCServerSIO.py Hmod.py
sed -i "s/MMVCServerSIO/Hmod/" Hmod.py
apt-get -y install libportaudio2 -qq
python3 -m pip install faiss-gpu fairseq pyngrok --quiet 
python3 -m pip install pyworld --no-build-isolation
python3 -m pip install -r requirements.txt --quiet
if [ ! -f "/kaggle/working/Hmod/server/stored_setting.json" ]; then
    wget -q https://gist.githubusercontent.com/Rafacasari/d820d945497a01112e1a9ba331cbad4f/raw/8e0a426c22688b05dd9c541648bceab27e422dd6/kaggle_setting.json -O /kaggle/working/24apuiBokE3TjZwc6tuqqv39SwP_2LRouVj3M9oZZCbzgntuG/server/stored_setting.json
fi

Token=$1
Region="ap" # 默认区域值

# 切换工作目录
cd /kaggle/working/Hmod/server

# 运行Python脚本
python3 - <<EOF
from pyngrok import conf, ngrok
import subprocess, threading, time, socket, urllib.request

# 设置Token和Region
MyConfig = conf.PyngrokConfig()
MyConfig.auth_token = "$Token"
MyConfig.region = "$Region"
conf.get_default().authtoken = "$Token"
conf.get_default().region = "$Region"
conf.set_default(MyConfig);

PORT = 8000
ngrokConnection = ngrok.connect(PORT)
public_url = ngrokConnection.public_url

def wait_for_server():
    while True:
        time.sleep(0.5)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('127.0.0.1', PORT))
        if result == 0:
            break
        sock.close()
    print("--------- 云端变声器服务已成功开启! ---------")
    print("点击下方链接，等待载入新页面后点击“Visit Site”即可进入变声器界面:")
    print(public_url)
    print("---------------------------------")

threading.Thread(target=wait_for_server, daemon=True).start()

# 运行Hmod.py脚本
subprocess.call([
    "python3", "Hmod.py",
    "-p", str(PORT),
    "--https", "False",
    "--content_vec_500", "pretrain/checkpoint_best_legacy_500.pt",
    "--content_vec_500_onnx", "pretrain/content_vec_500.onnx",
    "--content_vec_500_onnx_on", "true",
    "--hubert_base", "pretrain/hubert_base.pt",
    "--hubert_base_jp", "pretrain/rinna_hubert_base_jp.pt",
    "--hubert_soft", "pretrain/hubert/hubert-soft-0d54a1f4.pt",
    "--nsf_hifigan", "pretrain/nsf_hifigan/model",
    "--crepe_onnx_full", "pretrain/crepe_onnx_full.onnx",
    "--crepe_onnx_tiny", "pretrain/crepe_onnx_tiny.onnx",
    "--rmvpe", "pretrain/rmvpe.pt",
    "--model_dir", "model_dir",
    "--samples", "samples.json"
])

ngrok.disconnect(ngrokConnection.public_url)
EOF