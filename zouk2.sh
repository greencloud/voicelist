#!/bin/bash
cd /kaggle/working/
python3 -m pip install colorama --quiet
mkdir Hmod
cd Hmod
git clone https://github.com/w-okada/voice-changer.git --depth=1 --quiet .
cd server
sed -i "s/-.-.-.-/Kaggle.Mod/" '../client/demo/dist/assets/gui_settings/version.txt'
mv MMVCServerSIO.py Hmod.py
sed -i "s/MMVCServerSIO/Hmod/" Hmod.py
apt-get -y install libportaudio2 -qq
python3 -m pip install faiss-gpu fairseq pyngrok --quiet 
python3 -m pip install pyworld --no-build-isolation
python3 -m pip install -r requirements.txt --quiet
if [ ! -f "/kaggle/working/Hmod/server/stored_setting.json" ]; then
    wget -q https://gist.githubusercontent.com/Rafacasari/d820d945497a01112e1a9ba331cbad4f/raw/8e0a426c22688b05dd9c541648bceab27e422dd6/kaggle_setting.json -O /kaggle/working/24apuiBokE3TjZwc6tuqqv39SwP_2LRouVj3M9oZZCbzgntuG/server/stored_setting.json
fi
# --------------------------------------------------------------------------------------------------------------------------------------------------------
if [ ! -d "/kaggle/working/loophole-cli_1.0.0-beta.15_linux_64bit/" ]; then
    wget -q https://github.com/loophole/cli/releases/download/1.0.0-beta.15/loophole-cli_1.0.0-beta.15_linux_64bit.tar.gz -O /kaggle/working/1.tar.gz
    tar -xvf /kaggle/working/1.tar.gz -C /kaggle/working/
    rm -rf /kaggle/working/1.tar.gz
fi
echo -e "\033[32m> loophole down!\033[0m"    
chmod +x /kaggle/working/loophole-cli_1.0.0-beta.15_linux_64bit/loophole
echo -e "\033[32;1m> ---------------------------------------------------------------------------------------------\033[0m"
echo -e "\033[32;1m> 复制类似“RKZV-WWMT”格式的的黄色code（如果未出现，则忽略该条提示）\033[0m"
echo -e "\033[32;1m> 然后点击 https://loophole.eu.auth0.com/activate ，进入后根据提示填入code（需要注册并登录））\033[0m"
echo -e "\033[32;1m> ---------------------------------------------------------------------------------------------\033[0m"
echo
echo
echo
/kaggle/working/loophole-cli_1.0.0-beta.15_linux_64bit/loophole account login
# --------------------------------------------------------------------------------------------------------------------------------------------------------
cd /kaggle/working/Hmod/server
# 运行Python脚本
python3 - <<EOF
import os
import subprocess
subprocess.Popen("/kaggle/working/loophole-cli_1.0.0-beta.15_linux_64bit/loophole http 8000 --hostname 20240330zoukai", shell=True)
print("------------------------------------------------------------------------------------------------")
print("\033[1;31m" + "---------服务已开启,等待20秒点击 https://20240330zoukai.loophole.site\ 即可进入云端变声器！---------" + "\033[0m")
print("------------------------------------------------------------------------------------------------")
# 运行Hmod.py脚本
subprocess.call([
    "python3", "Hmod.py",
    "-p", "8000",
    "--https", "False",
    "--content_vec_500", "pretrain/checkpoint_best_legacy_500.pt",
    "--content_vec_500_onnx", "pretrain/content_vec_500.onnx",
    "--content_vec_500_onnx_on", "true",
    "--hubert_base", "pretrain/hubert_base.pt",
    "--hubert_base_jp", "pretrain/rinna_hubert_base_jp.pt",
    "--hubert_soft", "pretrain/hubert/hubert-soft-0d54a1f4.pt",
    "--nsf_hifigan", "pretrain/nsf_hifigan/model",
    "--crepe_onnx_full", "pretrain/crepe_onnx_full.onnx",
    "--crepe_onnx_tiny", "pretrain/crepe_onnx_tiny.onnx",
    "--rmvpe", "pretrain/rmvpe.pt",
    "--model_dir", "model_dir",
    "--samples", "samples.json"
])
EOF